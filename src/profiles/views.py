from django.contrib.auth import get_user_model
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import DetailView
from django.contrib.auth import logout

User = get_user_model()

class ProfileDetailView(DetailView):
	template_name = 'profiles/user.html'
	def get_object(self):
		username = self.kwargs.get("username")
		if username is None:
			raise Http404
		return get_object_or_404(User, username__iexact=username, is_active=True)

def logout_view(request):
    logout(request)
    return redirect('home-view')
# Create your views here.
