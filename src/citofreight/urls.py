"""citofreight URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from pages.views import home_view, about_view, contact_view
from vehicles_locations.views import LocationCreateView, LocationView #LocationsListView
from loadings_locations.views import (PickCreateView,
                                      PickView,
                                      DestinationCreateView,
                                      DestinationView)
from profiles.views import logout_view

from django.contrib.auth.views import LoginView


urlpatterns = [
    path('', home_view, name='home-view'),
    path('carriers/', include('carriers.urls')),
    path('senders/', include('senders.urls')),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', logout_view, name='logout'),
    path('admin/', admin.site.urls),
    path('about/', about_view, name='about-view'),
    path('contact/', contact_view, name='contact-view'),
    path('<str:username>/', include('profiles.urls')),
    path('locations/', include('vehicles_locations.urls')),
    path('senders/<str:user>/', include('loadings.urls')),
    path('senders/<str:user>/loadings/add/create pick/', PickCreateView.as_view(), name='pick-create' ),
    path('senders/<str:user>/loadings/add/create destination/', DestinationCreateView.as_view(), name='destination-create'),
    path('carriers/<str:user>/', include('vehicles.urls')),
    path('carriers/<str:user>/vehicles/<str:vehicle_number>/<int:location_id>/', LocationView.as_view(), name='location-detail'),
]
