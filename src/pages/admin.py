from django.contrib import admin

from carriers.models import Carrier
from vehicles.models import Fleet
from senders.models import SenderModel
from loadings.models import LoadingsModel

admin.site.register(Carrier)
admin.site.register(Fleet)
admin.site.register(SenderModel)
admin.site.register(LoadingsModel)

# Register your models here.
