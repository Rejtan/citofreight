
from django import forms

from .models import LoadingsModel

from carriers.models import Carrier

class CargoForm(forms.ModelForm):
    Neutral = 'Neutral'
    FMCG    = 'FMCG'
    ADR     = 'ADR'
    TYPES = (
        (Neutral,   'Neutral'),
        (FMCG,      'FMCG'),
        (ADR,       'ADR'),
    )
    pick_date   = forms.SplitDateTimeField(label='Pick Date', widget=forms.SplitDateTimeWidget)
    delivery_date   = forms.SplitDateTimeField(label='Delivery Date', widget=forms.SplitDateTimeWidget)
    title       = forms.CharField(max_length=40)
    types       = forms.ChoiceField(label='Type', choices=TYPES)
    length      = forms.IntegerField(label='Length', widget=forms.TextInput(attrs={"placeholder": "cm" }))
    width       = forms.IntegerField(label='Width', widget=forms.TextInput(attrs={"placeholder": "cm"}))
    high        = forms.IntegerField(label='High', widget=forms.TextInput(attrs={"placeholder": "cm"}))
    weight      = forms.IntegerField(label='Weight', widget=forms.TextInput(attrs={"placeholder": "kg"}))
    description = forms.CharField(label='Description', widget=forms.Textarea(attrs={'rows':10}))



    class Meta:
        model = LoadingsModel
        fields = [
            'pick_date',
            'delivery_date',
            'pick',
            'destination',
            'title',
            'types',
            'length',
            'width',
            'high',
            'weight',
            'description',
        ]

