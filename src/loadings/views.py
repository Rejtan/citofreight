from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404, redirect
from .models import LoadingsModel
from .forms import CargoForm
from django.views import View
from django.views.generic import ListView
from carriers.models import Carrier
from vehicles.models import Fleet
from django.db.models import Q



class CargoCreateView(LoginRequiredMixin, View):
    template_name = "loadings/cargo_create.html" # DetailView
    def get(self, request, *args, **kwargs):
        # GET method
        form = CargoForm()
        context = {"form": form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        # POST method
        form = CargoForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()
            form = CargoForm()
            return redirect('../')
        context = {"form": form}
        return render(request, self.template_name, context)

class CargoView(View):
    template_name = "loadings/cargo_detail.html" # DetailView
    def get(self, request, l_id, *args, **kwargs):
        # GET method
        context = {}
        if l_id is not None:
            obj = get_object_or_404(LoadingsModel, id=l_id)
            context['object'] = obj
        return render(request, self.template_name, context)


class CargoUpdateView(LoginRequiredMixin, View):
    template_name = "loadings/cargo_update.html" # DetailView
    def get_object(self):
        l_id = self.kwargs.get('l_id')
        obj = None
        if l_id is not None:
            obj = get_object_or_404(LoadingsModel, id=l_id)
        return obj

    def get(self, request, l_id, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = CargoForm(instance=obj)
            context['object'] = obj
            context['form'] = form
        return render(request, self.template_name, context)

    def post(self, request, l_id,  *args, **kwargs):
        # POST method
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = CargoForm(request.POST, instance=obj)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.user = request.user
                instance.save()                
                form = CargoForm()
                return redirect('../')
            context['object'] = obj
            context['form'] = form
        return render(request, self.template_name, context)


class CargoDeleteView(LoginRequiredMixin, View):
    template_name = "loadings/cargo_delete.html" # DetailView
    def get_object(self):
        l_id = self.kwargs.get('l_id')
        obj = None
        if l_id is not None:
            obj = get_object_or_404(LoadingsModel, id=l_id)
        return obj

    def get(self, request, l_id, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            context['object'] = obj
        return render(request, self.template_name, context)

    def post(self, request, l_id,  *args, **kwargs):
        # POST method
        context = {}
        obj = self.get_object()
        if obj is not None:
            obj.delete()
            context['object'] = None
            return redirect('../../')
        return render(request, self.template_name, context)


class FindingResultView(View):
    template_name = "loadings/results.html"
    queryset = Fleet.objects.all()
    # queryset = Carrier.objects.all()
    """docstring for FindingResultView"""
    def get_queryset(self):
        return self.queryset

    def get_object(self):
        l_id = self.kwargs.get('l_id')
        obj = None
        if l_id is not None:
            obj = get_object_or_404(LoadingsModel, id=l_id)
        return obj

    def get(self, request, *args, **kwargs):
        context = {}
        obj = self.get_object()
        if obj is not None:
            c_loc = obj.pick.city
            width = obj.width
            high = obj.high
            length = obj.length
            weight = obj.weight
            forwarders_list = Fleet.objects.filter(Q(location__city__iexact=c_loc) &
                                                             Q(width__gte=width) &
                                                             Q(high__gte=high) &
                                                             Q(length__gte=length) &
                                                             Q(capacity__gte=weight))
            context = {'object_list': forwarders_list}
        return render(request, "loadings/results.html", context)

class VehicleResultView(View):
    template_name = "loadings/results_detail.html" # DetailView
    def get(self, request, vehicle_number, *args, **kwargs):
        # GET method
        context = {}
        if vehicle_number is not None:
            obj = get_object_or_404(Fleet, register=vehicle_number)
            context['object'] = obj
        return render(request, self.template_name, context)


# class CarrierResultView(View):
#     template_name = "loadings/results_carrier.html"
#     def get(self, request, carrier_name, *args, **kwargs):
#         context = {}
#         if carrier_name is not None:
#             obj = get_object_or_404(Carrier, Name=carrier_name)
#             context['object'] = obj
#         return render(request, self.template_name, context)

       

""" Teraz muszę zdefiniowac pozostałe parametry i poprzez .exclude dodać kolejne stopnie filtrowania"""

    # class VehicleListView(View):
    #     template_name = "vehicles/cargo_list.html"
    #     queryset = Fleet.objects.all()
    #
    #     def get_queryset(self):
    #         return self.queryset
    #
    #     def get(self, request, *args, **kwargs):
    #         context = {'object_list': self.get_queryset()}
    #         return render(request, self.template_name, context)

# class SenderListView(ListView):
#     template_name = "senders/senders_list.html"
#     queryset = SenderModel.objects.all()

#     def sender_list_view(request, *args, **kwargs):
#         queryset = SenderModel.objects.all()
#         context = {
#             "object_list": queryset
#         }
#         return render(request, "senders/senders_list.html", context)


# class CarrierVehicleList(ListView):
#     template_name = "vehicles/vehicle_list.html"

#     def get_queryset(self):
#         self.carriers = get_object_or_404(Carrier, Name=self.kwargs['carrier_name'])
#         return Fleet.objects.filter(carriers=self.carriers)

# # Create your views here.

# >>> Entry.objects.filter(
# ...     headline__startswith='What'
# ... ).exclude(
# ...     pub_date__gte=datetime.date.today()
# ... ).filter(
# ...     pub_date__gte=datetime.date(2005, 1, 30)
# ... )