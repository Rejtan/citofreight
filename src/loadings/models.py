from django.conf import settings
from django.db import models
from senders.models import SenderModel
from loadings_locations.models import LoadingsLocationsModel, LoadingsDestinationModel

User = settings.AUTH_USER_MODEL

class LoadingsModel(models.Model):
    Neutral = 'Neutral'
    FMCG    = 'FMCG'
    ADR     = 'ADR'
    TYPES = (
        (Neutral, 'Neutral'),
        (FMCG,       'FMCG'),
        (ADR,    'Chłodnia'),
    )
    user        = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    pick_date   = models.DateTimeField(blank=False, null=True)
    delivery_date = models.DateTimeField(blank=False, null=True)
    title       = models.CharField(max_length=40)
    types       = models.CharField(max_length=40, choices=TYPES, default=Neutral)
    length      = models.PositiveIntegerField()
    width       = models.PositiveIntegerField()
    high        = models.PositiveIntegerField()
    weight      = models.PositiveIntegerField()
    description = models.TextField(blank=True)
    pick        = models.ForeignKey(LoadingsLocationsModel, on_delete=models.CASCADE, default=True)
    destination = models.ForeignKey(LoadingsDestinationModel, on_delete=models.CASCADE, default=False)



    def load_type(self):
        return self.TYPES in (self.type)


    def __str__(self):
        return "%s %s" % (self.title, self.types)


# Create your models here.
from django.db import models

# Create your models here.
