from django.urls import path, include


from loadings.views import (CargoCreateView,
                            CargoView,
                            CargoUpdateView,
                            CargoDeleteView,
                            FindingResultView,
                            VehicleResultView,)

from senders.views import SenderCargoList



app_name = 'loadings'
urlpatterns = [
    path('loadings/', SenderCargoList.as_view(), name='sender-cargo'),
    path('loadings/add/', CargoCreateView.as_view(), name='cargo-create'),
    path('loadings/<int:l_id>/', CargoView.as_view(), name='cargo-detail'),
    path('loadings/<int:l_id>/update/', CargoUpdateView.as_view(), name='cargo-update'),
    path('loadings/<int:l_id>/delete/', CargoDeleteView.as_view(), name='cargo-delete'),
    path('loadings/<int:l_id>/result/', FindingResultView.as_view(), name='transport-result'),
    path('loadings/<int:l_id>/result/<str:vehicle_number>/', VehicleResultView.as_view(), name='result-detail'),
    # path('loadings/<int:l_id>/result/<str:vehicle_number>/<str:carrier_name>', CarrierResultView.as_view(), name='deepresult-detail'),


]