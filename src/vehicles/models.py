from django.conf import settings
from django.db import models
from django.urls import reverse
from vehicles_locations.models import VehiclesLocationsModel

User = settings.AUTH_USER_MODEL

class Fleet(models.Model):
    FIRANKA = 'Firanka'
    PLANDEKA = 'Plandeka'
    CHLODNIA = 'Chłodnia'
    IZOTERMA = 'Izoterma'
    TYPES = (
        (FIRANKA, 'Firanka'),
        (PLANDEKA, 'Plandeka'),
        (CHLODNIA, 'Chłodnia'),
        (IZOTERMA, 'Izoterma'),
    )
    user        = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    types       = models.CharField(max_length=40, choices=TYPES, default=PLANDEKA)
    make        = models.CharField(max_length=40)
    model       = models.CharField(max_length=40)
    power       = models.PositiveIntegerField()
    length      = models.PositiveIntegerField()
    width       = models.PositiveIntegerField()
    high        = models.PositiveIntegerField()
    capacity    = models.PositiveIntegerField(null=True)
    register    = models.CharField(max_length=40)
    location    = models.ForeignKey(VehiclesLocationsModel, on_delete=models.CASCADE, default=False)


    def fleet_type(self):
        return self.TYPES in (self.type)


    # def get_absolute_url(self):
    #     return reverse("vehicles:vehicle-detail", kwargs={"vehicle_number": self.register})

    def __str__(self):
        return "%s %s %s" % (self.make, self.model, self.register)



# Create your models here.
