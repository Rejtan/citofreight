from django.urls import path, include

from vehicles.views import (#VehicleDetailView,
                            VehicleCreateView,
                            VehicleUpdateView,
                            #VehicleDeleteView,
                            #vehicle_delete_view,
                            VehicleView,
                            VehicleDeleteView,
                            #VehicleListView,
                            #vehicle_detail_view,)
                            )
from carriers.views import CarrierVehicleList
# vehicle_detail_view,  vehicle_list_view, vehicle_create_view,


app_name = 'vehicles'
urlpatterns = [
    path('vehicles/', CarrierVehicleList.as_view(), name='carrier-fleet'),
    path('vehicles/add/', VehicleCreateView.as_view(), name='vehicle-create'),
    # path('<int:id>/', VehicleDetailView.as_view(), name='vehicle-detail'),
    path('vehicles/<str:vehicle_number>/', VehicleView.as_view(), name='vehicle-detail'),
    path('vehicles/<str:vehicle_number>/update/', VehicleUpdateView.as_view(), name='vehicle-update'),
    path('vehicles/<str:vehicle_number>/delete/', VehicleDeleteView.as_view(), name='vehicle-delete'),
]