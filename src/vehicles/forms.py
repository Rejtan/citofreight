
from django import forms

from .models import Fleet

class FleetForm(forms.ModelForm):
    FIRANKA = 'Firanka'
    PLANDEKA = 'Plandeka'
    CHLODNIA = 'Chłodnia'
    IZOTERMA = 'Izoterma'
    TYPES = (
        (FIRANKA, 'Firanka'),
        (PLANDEKA, 'Plandeka'),
        (CHLODNIA, 'Chłodnia'),
        (IZOTERMA, 'Izoterma'),

    )
    types       = forms.ChoiceField(label='Type', choices=TYPES)
    make        = forms.CharField(label='Make')
    model       = forms.CharField(label='Model')
    power       = forms.IntegerField(label='Power', widget=forms.TextInput(attrs={"placeholder": "KM"}))
    length      = forms.IntegerField(label='Length', widget=forms.TextInput(attrs={"placeholder": "cm" }))
    width       = forms.IntegerField(label='Width', widget=forms.TextInput(attrs={"placeholder": "cm"}))
    high        = forms.IntegerField(label='High', widget=forms.TextInput(attrs={"placeholder": "cm"}))
    capacity    = forms.IntegerField(label='Capacity', widget=forms.TextInput(attrs={"placeholder": "kg"}))
    register    = forms.CharField(label='Register')
    # company     = forms.ModelChoiceField(queryset=Carrier.objects.all())



    class Meta:
        model = Fleet
        fields = [
            'types',
            'make',
            'model',
            'power',
            'length',
            'width',
            'high',
            'capacity',
            'register',
            'location',
        ]

