from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from .models import Fleet
from carriers.models import Carrier
from .forms import FleetForm
from django.views import View
# from django.views.generic import (
# DetailView,
# ListView,
# CreateView,
# UpdateView,
# DeleteView,
# )

########################################################################################################################
#MIXIN - tworzysz klasę, w której definiujesz powtarzalną część kodu, którą następnie możesz wykorzysztać w innych klasach.
#Nazwę klasy następnie definiujesz w liście argumentów klasy, w której chcesz zaimplementować kod

# Class VehicleObjectMixin(object):
#     model = Fleet
#     lookup = 'id'
#
#     def get_object(self):
#         id = self.kwargs.get(self.lookup)
#         obj = None
#         if id is not None:
#             obj = get_object_or_404(Fleet, id=id)
#         return obj
########################################################################################################################

# class VehicleListView(ListView):
#     template_name = "vehicles/cargo_list.html"
#     queryset = Fleet.objects.all()
#
#     def vehicle_list_view(request, *args, **kwargs):
#         queryset = Fleet.objects.all()
#         context = {
#             "object_list": queryset
#         }
#         return render(request, "vehicles/cargo_list.html", context)




    # def get_queryset(self):
    #     self.publisher = get_object_or_404(Publisher, name=self.kwargs['publisher'])
    #     return Book.objects.filter(publisher=self.publisher)

    # class VehicleListView(View):
    #     template_name = "vehicles/cargo_list.html"
    #     queryset = Fleet.objects.all()
    #
    #     def get_queryset(self):
    #         return self.queryset
    #
    #     def get(self, request, *args, **kwargs):
    #         context = {'object_list': self.get_queryset()}
    #         return render(request, self.template_name, context)



# class CarrierVehicleList(ListView):
#     template_name = "vehicles/cargo_list.html"
#     queryset = carrier.fleet_set
#
#     def vehicle_list_view(request, *args, **kwargs):
#         carrier = get
#         queryset = carrier.fleet_set
#         context = {
#             "object_list": queryset
#         }
#         return render(request, "vehicles/cargo_list.html", context)

# class CarrierVehicleList(ListView):
#     template_name = "vehicles/cargo_list.html"
#
#     def get_object(self):
#         carrier_name = self.kwargs.get('Name')
#         obj = None
#         if carrier_name is not None:
#             obj = get_object_or_404(Carrier, Name=carrier_name)
#         return obj
#
#     def get_queryset( request, *args, **kwargs):
#         context = {}
#             queryset = carrier.fleet_set
#             context = {'object_list': queryset}
#         return render(request, "vehicles/cargo_list.html", context)


class VehicleView(View):
    template_name = "vehicles/vehicle_detail.html" # DetailView
    def get(self, request, vehicle_number, *args, **kwargs):
        # GET method
        context = {}
        if vehicle_number is not None:
            obj = get_object_or_404(Fleet, register=vehicle_number)
            context={"object":obj}
        return render(request, self.template_name, context)


# class VehicleDetailView(DetailView):
#     template_name = "vehicles/cargo_detail.html"
#
#     def get_object(self):
#         vehicle_number = self.kwargs.get("register")
#         return get_object_or_404(Fleet, register=vehicle_number)


# def vehicle_detail_view(request, register, *args, **kwargs):
#     obj = get_object_or_404(Fleet, register=register)
#     context = {
#         "object": obj
#     }
#     return render(request, "vehicles/cargo_detail.html", context)








    # def get(self, request, id=None, *args, **kwargs):
    #     # GET method
    #     context = {}
    #     obj = self.get_object()
    #     if obj is not None:
    #         form = FleetForm(instance=obj)
    #         context['object'] = obj
    #         context['form'] = form
    #     return render(request, self.template_name, context)


# class VehicleListView(ListView):
#     template_name = "vehicles/cargo_list.html"
#     queryset = Fleet.objects.all()
#
#     def vehicle_list_view(request, *args, **kwargs):
#         queryset = Fleet.objects.all()
#         context = {
#             "object_list": queryset
#         }
#         return render(request, "vehicles/cargo_list.html", context)


#






class VehicleCreateView(LoginRequiredMixin, View):
    template_name = "vehicles/vehicle_create.html" # DetailView
    def get(self, request, *args, **kwargs):
        # GET method
        form = FleetForm()
        context = {"form": form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        # POST method
        form = FleetForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()
            form = FleetForm()
            return redirect('../')
        context = {"form": form}
        return render(request, self.template_name, context)




# class VehicleCreateView(CreateView):
#     template_name = "vehicles/cargo_create.html"
#     form_class = FleetForm
#     queryset = Fleet.objects.all()
#
#     def form_valid(self, form):
#         print(form.cleaned_data)
#         return super().form_valid(form)

# def vehicle_create_view(request, *args, **kwargs):
#     form = FleetForm(request.POST or None)
#     if form.is_valid():
#         form.save()
#         form = FleetForm()
#         return redirect('../')
#
#     context = {
#         'form': form
#
#     }
#     return render(request, "vehicles/cargo_create.html", context)

class VehicleUpdateView(LoginRequiredMixin, View):
    template_name = "vehicles/vehicle_update.html" # DetailView
    def get_object(self):
        vehicle_number = self.kwargs.get('vehicle_number')
        obj = None
        if vehicle_number is not None:
            obj = get_object_or_404(Fleet, register=vehicle_number)
        return obj

    def get(self, request, vehicle_number, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = FleetForm(instance=obj)
            context['object'] = obj
            context['form'] = form
        return render(request, self.template_name, context)

    def post(self, request, vehicle_number,  *args, **kwargs):
        # POST method
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = FleetForm(request.POST, instance=obj)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.user = request.user
                instance.save()
                form = FleetForm()
                return redirect('../')
            context['object'] = obj
            context['form'] = form
        return render(request, self.template_name, context)


# class VehicleUpdateView(UpdateView):
#     template_name = "vehicles/cargo_create.html"
#     form_class = FleetForm
#     queryset = Fleet.objects.all()
#
#     def get_object(self):
#         id_ = self.kwargs.get("id")
#         return get_object_or_404(Fleet, id=id_)
#
#     def form_valid(self, form):
#         print(form.cleaned_data)
#         return super().form_valid(form)



# class VehicleDeleteView(DeleteView):
#     template_name = "vehicles/cargo_delete.html"
#
#     def get_object(self):
#         id_ = self.kwargs.get("id")
#         return get_object_or_404(Fleet, id=id_)
#
#     def get_succes_url(self):
#         return reverse('vehicles:vehicle-list')

class VehicleDeleteView(LoginRequiredMixin, View):
    template_name = "vehicles/vehicle_delete.html" # DetailView
    def get_object(self):
        vehicle_number = self.kwargs.get('vehicle_number')
        obj = None
        if vehicle_number is not None:
            obj = get_object_or_404(Fleet, register=vehicle_number)
        return obj

    def get(self, request, vehicle_number, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            context['object'] = obj
        return render(request, self.template_name, context)

    def post(self, request, vehicle_number,  *args, **kwargs):
        # POST method
        context = {}
        obj = self.get_object()
        if obj is not None:
            obj.delete()
            context['object'] = None
            return redirect('../../')
        return render(request, self.template_name, context)




# def vehicle_delete_view(request, id):
#     obj = Fleet.objects.get(id=id)
#     # POST request
#     if request.method == "POST":
#         obj.delete()
#         return redirect('../../')
#     context = {
#         "object": obj
#     }
#     return render(request, "vehicles/cargo_delete.html", context)



# Create your views here.
