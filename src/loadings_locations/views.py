from django.shortcuts import render, get_object_or_404, redirect
from .models import LoadingsDestinationModel, LoadingsLocationsModel
from .forms import LoadingsLocationsModelForm, LoadingsDestinationModelForm
from django.views import View
from django.views.generic import ListView

class PickCreateView(View):
    template_name = "loadings_locations/pick_create.html" # DetailView
    def get(self, request, *args, **kwargs):
        # GET method
        form = LoadingsLocationsModelForm()
        context = {"form": form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        # POST method
        form = LoadingsLocationsModelForm(request.POST)
        if form.is_valid():
            form.save()
            form = LoadingsLocationsModelForm()
            return redirect('../')
        context = {"form": form}
        return render(request, self.template_name, context)

class PickView(View):
    template_name = "loadings_locations/pick_detail.html" # DetailView
    def get(self, request, location_id, *args, **kwargs):
        # GET method
        context = {}
        if location_id is not None:
            obj = get_object_or_404(LoadingsLocationsModel, id=location_id)
            context['object'] = obj
        return render(request, self.template_name, context)
#
#
class PickList(ListView):
    template_name = "loadings_locations/pick_list.html"
    queryset = LoadingsLocationsModel.objects.all()

    def locations_list_view(request, *args, **kwargs):
        queryset = LoadingsLocationsModel.objects.all()
        context = {
            "object_list": queryset
        }
        return render(request, "loadings_locations/pick_list.html", context)


class DestinationCreateView(View):
    template_name = "loadings_locations/destination_create.html" # DetailView
    def get(self, request, *args, **kwargs):
        # GET method
        form = LoadingsDestinationModelForm()
        context = {"form": form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        # POST method
        form = LoadingsDestinationModelForm(request.POST)
        if form.is_valid():
            form.save()
            form = LoadingsDestinationModelForm()
            return redirect('../')
        context = {"form": form}
        return render(request, self.template_name, context)

class DestinationView(View):
    template_name = "loadings_locations/destination_detail.html" # DetailView
    def get(self, request, location_id, *args, **kwargs):
        # GET method
        context = {}
        if location_id is not None:
            obj = get_object_or_404(LoadingsDestinationModel, id=location_id)
            context['object'] = obj
        return render(request, self.template_name, context)
#
#
class DestinationsList(ListView):
    template_name = "loadings_locations/destination_list.html"
    queryset = LoadingsDestinationModel.objects.all()

    def locations_list_view(request, *args, **kwargs):
        queryset = LoadingsDestinationModel.objects.all()
        context = {
            "object_list": queryset
        }
        return render(request, "loadings_locations/destination_list.html", context)
# # Create your views here.
