from django.db import models

class LoadingsLocationsModel(models.Model):

    country         = models.CharField(max_length=20)
    city            = models.CharField(max_length=20)
    postalcode      = models.CharField(max_length=10)
    street          = models.CharField(max_length=20)

    def __str__(self):
        return "%s, %s %s, %s" % (self.country, self.postalcode, self.city, self.street)

class LoadingsDestinationModel(models.Model):

    country         = models.CharField(max_length=20)
    city            = models.CharField(max_length=20)
    postalcode      = models.CharField(max_length=10)
    street          = models.CharField(max_length=20)

    def __str__(self):
        return "%s, %s %s, %s" % (self.country, self.postalcode, self.city, self.street)
# Create your models here.
