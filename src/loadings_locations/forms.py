from django import forms

from .models import LoadingsLocationsModel, LoadingsDestinationModel

class LoadingsLocationsModelForm(forms.ModelForm):

    country         = forms.CharField(label='Country', widget=forms.TextInput(attrs={"placeholder": "Country"}))
    city            = forms.CharField(label='City', widget=forms.TextInput(attrs={"placeholder": "City"}))
    postalcode      = forms.CharField(label='Postal Code', widget=forms.TextInput(attrs={"placeholder": "Postal Code"}))
    street          = forms.CharField(label='Street', widget=forms.TextInput(attrs={"placeholder": "Street"}))


    class Meta:
        model = LoadingsLocationsModel
        fields = [
            'country',
            'city',
            'postalcode',
            'street',

        ]


class LoadingsDestinationModelForm(forms.ModelForm):

    country         = forms.CharField(label='Country', widget=forms.TextInput(attrs={"placeholder": "Country"}))
    city            = forms.CharField(label='City', widget=forms.TextInput(attrs={"placeholder": "City"}))
    postalcode      = forms.CharField(label='Postal Code', widget=forms.TextInput(attrs={"placeholder": "Postal Code"}))
    street          = forms.CharField(label='Street', widget=forms.TextInput(attrs={"placeholder": "Street"}))
  


    class Meta:
        model = LoadingsDestinationModel
        fields = [
            'country',
            'city',
            'postalcode',
            'street',

        ]
