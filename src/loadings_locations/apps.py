from django.apps import AppConfig


class LoadingsLocationsConfig(AppConfig):
    name = 'loadings_locations'
