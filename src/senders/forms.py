
from django import forms

from .models import SenderModel

from carriers.models import Carrier

class SenderModelForm(forms.ModelForm):

    name        = forms.CharField(label='Name', widget=forms.TextInput(attrs={"placeholder": "Enter your company name"}))
    country     = forms.CharField(label='Country', widget=forms.TextInput(attrs={"placeholder": "Country"}))
    city        = forms.CharField(label='City', widget=forms.TextInput(attrs={"placeholder": "City"}))
    postalcode  = forms.CharField(label='PostalCode', widget=forms.TextInput(attrs={"placeholder": "Postal Code" }))
    street      = forms.CharField(label='Steet', widget=forms.TextInput(attrs={"placeholder": "Street"}))
    email       = forms.EmailField(label='Email', widget=forms.TextInput(attrs={"placeholder": "e-mail"}))
    description = forms.CharField(label='Description', widget=forms.TextInput(attrs={"placeholder": "Let us introducing you"}))

    class Meta:
        model = SenderModel
        fields = [
            'name',
            'country',
            'city',
            'postalcode',
            'street',
            'email',
            'description'
        ]

