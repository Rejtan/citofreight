from django.urls import path, include


from senders.views import (SenderCreateView,
                           SenderListView,
                           SenderView,
                           SenderDeleteView,
                           SenderUpdateView)




app_name = 'senders'
urlpatterns = [
    path('', SenderListView.as_view(), name='sender-list'),
    path('create/', SenderCreateView.as_view(), name='sender-create'),
    path('<str:user>/', SenderView.as_view(), name='sender-detail'),
    path('<str:user>/delete/', SenderDeleteView.as_view(), name='sender-delete'),
    path('<str:user>/update/', SenderUpdateView.as_view(), name='sender-update'),

]


# SenderCargoList