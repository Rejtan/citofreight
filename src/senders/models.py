from django.conf import settings
from django.db import models
from django.urls import reverse

User = settings.AUTH_USER_MODEL

class SenderModel(models.Model):
    user        = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    name        = models.CharField(max_length=120)
    country     = models.CharField(max_length=120)
    city        = models.CharField(max_length=120)
    postalcode  = models.CharField(max_length=10)
    street      = models.CharField(max_length=120)
    email       = models.EmailField(blank=False, null=True)
    description = models.TextField()

    class Meta:
        ordering = ["-user"]

    def __str__(self):
        return "%s" % (self.name)

    def get_absolute_url(self):
        return reverse("senders:sender-detail", kwargs={"user": self.user})


# Create your models here.
