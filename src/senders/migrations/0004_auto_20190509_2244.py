# Generated by Django 2.2 on 2019-05-09 20:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('senders', '0003_sendermodel_user'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sendermodel',
            options={'ordering': ['-user']},
        ),
    ]
