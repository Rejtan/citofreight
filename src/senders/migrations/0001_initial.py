# Generated by Django 2.2 on 2019-04-17 17:27

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SenderModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120)),
                ('country', models.CharField(max_length=120)),
                ('city', models.CharField(max_length=120)),
                ('postalcode', models.CharField(max_length=10)),
                ('street', models.CharField(max_length=120)),
                ('housenumber', models.PositiveSmallIntegerField()),
                ('email', models.EmailField(max_length=254, null=True)),
                ('description', models.TextField()),
            ],
            options={
                'ordering': ['-name'],
            },
        ),
    ]
