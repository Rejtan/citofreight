from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from .models import SenderModel
from django.views import View
from .forms import SenderModelForm
from django.views.generic import ListView
from loadings.models import LoadingsModel


class SenderListView(LoginRequiredMixin, ListView):
    template_name = "senders/senders_list.html"
    def get_queryset(self):
        return SenderModel.objects.filter(user=self.request.user)

class SenderCreateView(LoginRequiredMixin, View):
    template_name = "senders/sender_create.html"

    def get(self, request, *args, **kwargs):
        form = SenderModelForm()
        context = {"form": form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = SenderModelForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()
            form = SenderModelForm()
            return redirect('../')
        context = {"form": form}
        return render(request, self.template_name, context)


class SenderView(LoginRequiredMixin, View):
    template_name = "senders/sender_detail.html" # DetailView

    def get(self, request, user, *args, **kwargs):
        # GET method
        context = {}
        if user is not None:
            obj = get_object_or_404(SenderModel, user=self.request.user)
            context['object'] = obj
        return render(request, self.template_name, context)


class SenderDeleteView(LoginRequiredMixin, View):
    template_name = "senders/sender_delete.html" # DetailView

    def get_object(self):
        user = self.kwargs.get('user')
        obj = None
        if user is not None:
            obj = get_object_or_404(SenderModel, user=self.request.user)
        return obj

    def get(self, request, user, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            context['object'] = obj
        return render(request, self.template_name, context)

    def post(self, request, user, *args, **kwargs):
        # POST method
        context = {}
        obj = self.get_object()
        if obj is not None:
            obj.delete()
            context['object'] = None
            return redirect('../../')
        return render(request, self.template_name, context)

class SenderUpdateView(LoginRequiredMixin, View):
    template_name = "senders/sender_update.html" # DetailView
    def get_object(self):
        user = self.kwargs.get('user')
        obj = None
        if user is not None:
            obj = get_object_or_404(SenderModel, user=self.request.user)
        return obj

    def get(self, request, user, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = SenderModelForm(instance=obj)
            context['object'] = obj
            context['form'] = form
        return render(request, self.template_name, context)

    def post(self, request, user,  *args, **kwargs):
        # POST method
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = SenderModelForm(request.POST, instance=obj)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.user = request.user
                instance.save()
                form = SenderModelForm()
                return redirect('../')
            context['object'] = obj
            context['form'] = form
        return render(request, self.template_name, context)


class SenderCargoList(LoginRequiredMixin, ListView):
    template_name = "loadings/cargo_list.html"
    def get_queryset(self):
        return LoadingsModel.objects.filter(user=self.request.user)


# Create your views here.
