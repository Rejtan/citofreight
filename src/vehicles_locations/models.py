from django.db import models

class VehiclesLocationsModel(models.Model):

    country         = models.CharField(max_length=20)
    city            = models.CharField(max_length=20)
    postalcode      = models.CharField(max_length=10)
    street          = models.CharField(max_length=20)
    housenumber     = models.PositiveSmallIntegerField()

    def __str__(self):
        return "%s %s %s %s %s" % (self.country, self.city, self.postalcode, self.street, self.housenumber)

# Create your models here.
from django.db import models

# Create your models here.
