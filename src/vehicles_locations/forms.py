from django import forms

from .models import VehiclesLocationsModel

class VehiclesLocationsModelForm(forms.ModelForm):

    country         = forms.CharField(label='Country', widget=forms.TextInput(attrs={"placeholder": "Country"}))
    city            = forms.CharField(label='City', widget=forms.TextInput(attrs={"placeholder": "City"}))
    postalcode      = forms.CharField(label='Postal Code', widget=forms.TextInput(attrs={"placeholder": "Postal Code"}))
    street          = forms.CharField(label='Street', widget=forms.TextInput(attrs={"placeholder": "Street"}))
    housenumber     = forms.IntegerField(label='House Number', widget=forms.TextInput(attrs={"placeholder": "House Number"}))


    class Meta:
        model = VehiclesLocationsModel
        fields = [
            'country',
            'city',
            'postalcode',
            'street',
            'housenumber',
        ]

