from django.shortcuts import render, get_object_or_404, redirect
from .models import VehiclesLocationsModel
from .forms import VehiclesLocationsModelForm
from django.views import View
from django.views.generic import ListView

class LocationCreateView(View):
    template_name = "vehicles_locations/location_create.html" # DetailView
    def get(self, request, *args, **kwargs):
        # GET method
        form = VehiclesLocationsModelForm()
        context = {"form": form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        # POST method
        form = VehiclesLocationsModelForm(request.POST)
        if form.is_valid():
            form.save()
            form = VehiclesLocationsModelForm()
            return redirect('../')
        context = {"form": form}
        return render(request, self.template_name, context)

class LocationView(View):
    template_name = "vehicles_locations/location_detail.html" # DetailView
    def get(self, request, location_id, *args, **kwargs):
        # GET method
        context = {}
        if location_id is not None:
            obj = get_object_or_404(VehiclesLocationsModel, id=location_id)
            context['object'] = obj
        return render(request, self.template_name, context)
#
#
class LocationsListView(ListView):
    template_name = "vehicles_locations/location_list.html"
    queryset = VehiclesLocationsModel.objects.all()

    def locations_list_view(request, *args, **kwargs):
        queryset = VehiclesLocationsModel.objects.all()
        context = {
            "object_list": queryset
        }
        return render(request, "vehicles_locations/location_list.html", context)
# # Create your views here.
