from django.apps import AppConfig


class VehiclesLocationosConfig(AppConfig):
    name = 'vehicles_locationos'
