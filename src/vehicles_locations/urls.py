from django.urls import path, include

from vehicles_locations.views import LocationCreateView, LocationView, LocationsListView


app_name = 'vehicles_locations'
urlpatterns = [
    #path('<int:location_id>/', LocationView.as_view(), name='location_detail'),
    path('', LocationsListView.as_view(), name='location-list'),
    path('create/', LocationCreateView.as_view(), name='location-create'),
]