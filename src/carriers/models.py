from django.conf import settings
from django.db import models
from django.urls import reverse

User = settings.AUTH_USER_MODEL

class Carrier(models.Model):
    user        = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    Name        = models.CharField(max_length=120)
    Country     = models.CharField(max_length=120)
    City        = models.CharField(max_length=120)
    PostalCode  = models.CharField(max_length=10)
    Street      = models.CharField(max_length=120)
    HouseNumber = models.PositiveSmallIntegerField()
    Email       = models.EmailField(blank=False, null=True)
    StartDate   = models.DateField(blank=False, null=True)
    Documents   = models.FileField(blank=True, null=True)

    class Meta:
        ordering = ["-user"]


    def __str__(self):
        return "%s" % (self.Name)


    def get_absolute_url(self):
        return reverse("carriers:carrier-detail", kwargs={"user": self.user})




# Create your models here.
