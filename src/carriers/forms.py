from django import forms

from .models import Carrier

class CarrierForm(forms.ModelForm):
    Name       = forms.CharField(label='Name', widget=forms.TextInput(attrs={"placeholder": "Enter your company name"}))
    Country     = forms.CharField(label='Country', widget=forms.TextInput(attrs={"placeholder": "Country"}))
    City        = forms.CharField(label='City', widget=forms.TextInput(attrs={"placeholder": "City"}))
    PostalCode  = forms.CharField(label='PostalCode', widget=forms.TextInput(attrs={"placeholder": "Postal Code"}))
    Street      = forms.CharField(label='Street', widget=forms.TextInput(attrs={"placeholder": "Street"}))
    HouseNumber = forms.IntegerField(label='HouseNumber', widget=forms.TextInput(attrs={"placeholder": "House Number"}))
    Email       = forms.EmailField(label='Email', widget=forms.TextInput(attrs={"placeholder": "e-mail"}))
    StartDate   = forms.DateField(label='StartDate', widget=forms.TextInput(attrs={"placeholder": "RRRR-MM-DD"}))
    Documents   = forms.FileField(label='Documents', required=False)

    class Meta:
        model = Carrier
        fields = [
            'Name',
            'Country',
            'City',
            'PostalCode',
            'Street',
            'HouseNumber',
            'Email',
            'StartDate',
            'Documents'
        ]