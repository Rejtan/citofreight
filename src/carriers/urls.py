from django.urls import path


from carriers.views import CarrierView, carrier_create_view, CarrierListView, CarrierDeleteView, CarrierUpdateView


app_name = 'carriers'
urlpatterns = [
path('', CarrierListView.as_view(), name='carrier-list'),
path('create/', carrier_create_view, name='create'),
path('<str:user>/', CarrierView.as_view(), name='carrier-detail'),
path('<str:user>/delete/', CarrierDeleteView.as_view(), name='carrier-delete'),
path('<str:user>/update/', CarrierUpdateView.as_view(), name='carrier-update'),

]