from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, UpdateView, DeleteView
from django.views import View
from .models import Carrier
from vehicles.models import Fleet
from .forms import CarrierForm


class CarrierDeleteView(LoginRequiredMixin, View):
    template_name = "carriers/carrier_delete.html" # DetailView

    def get_object(self):
        user = self.kwargs.get('user')
        obj = None
        if user is not None:
            obj = get_object_or_404(Carrier, user=self.request.user)
        return obj

    def get(self, request, user, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            context['object'] = obj
        return render(request, self.template_name, context)

    def post(self, request, user, *args, **kwargs):
        # POST method
        context = {}
        obj = self.get_object()
        if obj is not None:
            obj.delete()
            context['object'] = None
            return redirect('../../')
        return render(request, self.template_name, context)


class CarrierListView(LoginRequiredMixin, ListView):
    template_name = "carriers/carrier_list.html"
    def get_queryset(self):
        return Carrier.objects.filter(user=self.request.user)


@login_required
def carrier_create_view(request, *args, **kwargs):
    form = CarrierForm(request.POST or None)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.user = request.user
        obj.save()
        form = CarrierForm()
        return redirect('../')

    context = {
        'form': form

    }
    return render(request, "carriers/carrier_create.html", context)


class CarrierUpdateView(LoginRequiredMixin, View):
    template_name = "carriers/carrier_update.html" # DetailView
    def get_object(self):
        user = self.kwargs.get('user')
        obj = None
        if user is not None:
            obj = get_object_or_404(Carrier, user=self.request.user)
        return obj

    def get(self, request, *args, **kwargs):
        # GET method
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = CarrierForm(instance=obj)
            context['object'] = obj
            context['form'] = form
        return render(request, self.template_name, context)

    def post(self, request,   *args, **kwargs):
        # POST method
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = CarrierForm(request.POST, instance=obj)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.user = request.user
                instance.save()
                form = CarrierForm()
                return redirect('../')
            context['object'] = obj
            context['form'] = form
        return render(request, self.template_name, context)


# @login_required
# def dynamic_lookup_view(request, self, user, *args, **kwargs):
#     obj = get_object_or_404(Carrier, user=self.request.user)
#     context={
#         "object": obj
#     }
#     return render(request, "carriers/carrier_detail.html", context)

class CarrierView(LoginRequiredMixin, View):
    template_name = "carriers/carrier_detail.html" # DetailView

    def get(self, request, user, *args, **kwargs):
        # GET method
        context = {}
        if user is not None:
            obj = get_object_or_404(Carrier, user=self.request.user)
            context['object'] = obj
        return render(request, self.template_name, context)

class CarrierVehicleList(LoginRequiredMixin, ListView):
    template_name = "vehicles/vehicle_list.html"
    def get_queryset(self):
        return Fleet.objects.filter(user=self.request.user)

# def carrier_detail_view(request, *args, **kwargs):
#     obj = Carrier.objects.get(id=1)
#
#     context = {
#         'object': obj
#     }
#     return render(request, "carriers/carrier_detail.html", context)


# Create your views here.
